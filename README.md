# DOCKER
Projeto do curso de Docker

Principais comandos usados no docker e docker-compose

## Docker commands
- Comandos relacionados às informações
  
  |  COMANDO                    | DESCRIÇÃO                                                                   |
  |-----------------------------|-----------------------------------------------------------------------------|
  | docker version              | Exibe a versão do docker.                                                   |
  | docker inspect ID_CONTAINER | Retorna diversas informações sobre o container.                             |
  | docker ps                   | Exibe todos os containers em execução no momento.                           |
  | docker ps -a                | Exibe todos os containers, independentemente de estarem em execução ou não. |

- Comandos relacionados à execução
  
  |  COMANDO                                                                   | DESCRIÇÃO                                                                                 |
  |----------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
  | docker run NOME_DA_IMAGEM                                                  | Cria um container com a respectiva imagem passada como parâmetro.                         |
  | docker run -it NOME_DA_IMAGEM                                              | Conecta o terminal que estamos utilizando com o do container.                             |
  | docker run -d -P --name NOME NOME_DA_IMAGEM                                | Ao executar, dá um nome ao container.                                                     |
  | docker run -d -p 12345:80 NOME_DA_IMAGEM                                   | Define uma porta específica para ser atribuída à porta 80 do container, neste caso 12345. |
  | docker run -v "CAMINHO_VOLUME" NOME_DA_IMAGEM                              | Cria um volume no respectivo caminho do container.                                        |
  | docker run -it --name NOME_CONTAINER --network NOME_DA_REDE NOME_DA_IMAGEM | Cria um container especificando seu nome e qual rede deverá ser usada.                    |
  | docker run -d -P -e AUTHOR="Fulano" NOME_DA_IMAGEM                         | Define uma variável de ambiente AUTHOR com o valor Fulano no container criado.            |
  | docker exec -it NOME_CONTAINER ping NOME_CONTAINER_ALVO                    | Executa o comando ping NOME_CONTAINER dentro do container NOME_CONTAINER_ALVO             |

- Comandos relacionados à inicialização/interrupção
  
  |  COMANDO                        | DESCRIÇÃO                                                                                            |
  |---------------------------------|------------------------------------------------------------------------------------------------------|
  | docker start ID_CONTAINER       | Inicia o container com id em questão.                                                                |
  | docker start -a -i ID_CONTAINER | Inicia o container com id em questão e integra os terminais, além de permitir interação entre ambos. |
  | docker stop ID_CONTAINER        | Interrompe o container com id em questão.                                                            |

- Comandos relacionados à remoção
  
  |  COMANDO                  | DESCRIÇÃO                                     |
  |---------------------------|-----------------------------------------------|
  | docker rm ID_CONTAINER    | Remove o container com id em questão.         |
  | docker container prune    | Remove todos os containers que estão parados. |
  | docker rmi NOME_DA_IMAGEM | Remove a imagem passada como parâmetro.       |

- Comandos relacionados à construção de Dockerfile
  
  |  COMANDO                                                                  | DESCRIÇÃO                                                                        |
  |---------------------------------------------------------------------------|----------------------------------------------------------------------------------|
  | docker build -f Dockerfile                                                | Cria uma imagem a partir de um Dockerfile.                                       |
  | docker build -f Dockerfile -t NOME_USUARIO/NOME_IMAGEM                    | Constrói e nomeia uma imagem não-oficial.                                        |
  | docker build -f CAMINHO_DOCKERFILE/Dockerfile -t NOME_USUARIO/NOME_IMAGEM | Constrói e nomeia uma imagem não-oficial informando o caminho para o Dockerfile. |

- Comandos relacionados ao Docker Hub
  
  |  COMANDO                             | DESCRIÇÃO                                 |
  |--------------------------------------|------------------------------------------ |
  | docker login                         | Inicia o processo de login no Docker Hub. |
  | docker push NOME_USUARIO/NOME_IMAGEM | Envia a imagem criada para o Docker Hub.  |
  | docker pull NOME_USUARIO/NOME_IMAGEM | Baixa a imagem desejada do Docker Hub.    |

- Comandos relacionados à rede
  
  |  COMANDO                                           | DESCRIÇÃO                                      |
  |----------------------------------------------------|------------------------------------------------| 
  | docker network create --driver bridge NOME_DA_REDE | Cria uma rede especificando o driver desejado. | 

## Docker Compose

  |  COMANDO             | DESCRIÇÃO                                     |
  |----------------------|-----------------------------------------------|
  | docker-compose build | Realiza o build baseado no docker-compose.yml |
  | docker-compose up    | Sobe os serviços criados.                     |
  | docker-compose up -d | Sobe os serviços criados.                     |
  | docker-compose down  | para os serviços criados.                     |
  | docker-compose ps    | lista os serviços que estão rodando.          |
